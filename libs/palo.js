let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest
let md5 = require('md5')

function makeRequest(method, url) {
	return new Promise(function (resolve, reject) {
		let xhr = new XMLHttpRequest();
		xhr.open(method, url);
		console.log('----','url: ', url)
		xhr.onload = function () {		          
			const err = parseInt(xhr.responseText.substring(0,4))
			if(err && err>1000 && xhr.responseText[4]==';'){
				console.log('palo error: ', xhr.responseText)
				const res = xhr.responseText.replace(/"/g, "").split(';')
				reject({status: 400, message: res[1]+'; '+res[2], errCode: err })
			}
			resolve(xhr.responseText)
		};
		xhr.onerror = function () {
			reject(xhr.responseText)
		};
		xhr.send();
	});
}

function test(){
	return('good')
}

module.exports = {
	getPaloAPI: async function (ctx, apiFunc, getSid, server){
		try{
			let sid = ctx.session.sid || ctx.request.body.sid
			if(ctx.request.body.login && ctx.request.body.pass){
				sid = await getSid(ctx.request.body.login, ctx.request.body.pass, server);
			}
			const res = await apiFunc(sid)
			return res
		}catch(e){
			console.log('e: ', e)
			if(e.errCode == 1015){
				if(ctx.session.user && ctx.session.pass){
					ctx.session.sid = await getSid(ctx.session.user, ctx.session.pass, server);
					const res = await apiFunc(ctx.session.sid)
					return res
				}else{  
					throw ({ status: 401, message: 'Authentication required' }) 
				}
			}else if(e.errCode == 1000){
				return []
			}else if(e.errCode == 1004){
				ctx.session.sid = ''
				ctx.session.user = ''
				ctx.session.pass = ''
				throw ({ status: 401, message: e.message })
			}else if(e.errCode == 5005){
				return true
			}
			else{
				console.log('else')
				throw ({ status: 400, message: e.message })	
			}
		}
		
	},



	getSid: async function (login, pass, server){
		const url = server+'/server/login?user='+login+'&password='+pass
		const response = await makeRequest("GET", url)
		const sid = response.split(";")[0];
		return sid
	},
	
	getDBList: async function (sid, server){
		console.log('palo sid: ', sid)
		console.log('palo server: ', server)
		let url = server + '/server/databases?sid='+sid
		let response = await makeRequest("GET", url)
		let responseArray = response.split('\n');
		let databases = [];
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let db = {
				id: str[0],
				name: str[1],
				number_dimension: str[2],
				number_cubes: str[3],
				status: str[4]
			}
			if (db.id) {databases.push(db)}
		}
		databases = databases.filter(db => db != null)
		return databases	
	},
	
	getDimList: async function (sid, db, showSystem, showNormal, showAttribute, showInfo, showPermission, server){
		let url = server + '/database/dimensions?sid='+sid+'&database='+db+'&show_system='+showSystem+'&show_normal='+showNormal+'&show_attribute='+showAttribute+'&show_info='+showInfo+'&show_permission='+showPermission;
		let response = await makeRequest("GET", url)
		let dimensions = []
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dim = {
				id: str[0],
				name: str[1],
				elementNumber: str[2],
				maximumLevel: str[3],
				maximumIndent: str[4],
				maximumDepth: str[5],
				type: str[6],
				attributesDim: str[7],
				attributesCube: str[8],
				rightsCube: str[9],
				dimToken: str[10],
				permsission: str[11]
			}
			if (dim.id) {
				dim.name = dim.name.replace(/"/g, "");
				dimensions[dim.id]=dim
			}
		}
		dimensions = dimensions.filter( e => e != null)
		return dimensions
	},

	getDimByName: async function (sid, db, dimName, server) {
		let url = server + '/database/dimensions?sid='+sid+'&database='+db;
		let response = await makeRequest("GET", url)
		let dimensions = []
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dim = {
				id: str[0],
				name: str[1],
				elementNumber: str[2],
				maximumLevel: str[3],
				maximumIndent: str[4],
				maximumDepth: str[5],
				type: str[6],
				attributesDim: str[7],
				attributesCube: str[8],
				rightsCube: str[9],
				dimToken: str[10],
				permsission: str[11],
				elements: [],
				elementsById: [],
				elementsByName: [],
				filters: []
			}
			if (dim.id) {
				dim.name = dim.name.replace(/"/g, "");
				dimensions[dim.id]=dim
			}
		}
		dim = dimensions.filter( e => e.name == dimName)
		dim = dim[0]
		return dim
	},

	getDimById: async function (sid, db, dimId, server) {
		let url = server + '/database/dimensions?sid='+sid+'&database='+db;
		let response = await makeRequest("GET", url)
		let dimensions = []
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dim = {
				id: str[0],
				name: str[1],
				elementNumber: str[2],
				maximumLevel: str[3],
				maximumIndent: str[4],
				maximumDepth: str[5],
				type: str[6],
				attributesDim: str[7],
				attributesCube: str[8],
				rightsCube: str[9],
				dimToken: str[10],
				permsission: str[11],
				elements: [],
				elementsById: [],
				elementsByName: [],
				filters: []
			}
			if (dim.id) {
				dim.name = dim.name.replace(/"/g, "");
				dimensions[dim.id]=dim
			}
		}
		dim = dimensions.filter( e => e.id == dimId)
		dim = dim[0]
		return dim
	},

	getDimElements: async function (sid, db, dimId, show_permission, server) {
		url = server+'/dimension/elements?sid='+sid+'&database='+db+'&dimension='+dimId+'&show_permission='+show_permission
		response = await makeRequest("GET", url)
		let elements = []
		responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			if (str[1]){
				let el = {
					id: parseInt(str[0]),
					name: str[1],
					position: str[2],
					level: str[3],
					indent: str[4],
					depth: str[5],
					type: str[6],
					numberParents: str[7],
					parents: str[8],
					numberChildren: str[9],
					children: str[10],
					weights: str[11],
					permission: str[12]
				}
				el.name = el.name.replace(/"/g, "");
				el.permission = el.permission.replace(/"/g, "");
				elements.push(el);
			}
		}
		return elements
	},

	getDimsElements: async function (sid, db, dimList, server) {
		let dimsElements = []

		for(let d in dimList){
			let dimId = dimList[d]
			url = server+'/dimension/elements?sid='+sid+'&database='+db+'&dimension='+dimId
			response = await makeRequest("GET", url)
			let elements = []
			responseArray = response.split('\n');
			for (let i in responseArray){
				let str = responseArray[i].split(";");
				if (str[1]){
					let el = {
						id: parseInt(str[0]),
						name: str[1],
						position: str[2],
						level: str[3],
						indent: str[4],
						depth: str[5],
						type: str[6],
						numberParents: str[7],
						parents: str[8],
						numberChildren: str[9],
						children: str[10],
						weights: str[11],
						permission: str[12]
					}
					el.name = el.name.replace(/"/g, "");
					//elements.push(el);
					elements[el.id] = el
				}
			}
			dimsElements[dimId] = elements
		}
		return dimsElements
	},

	getCubeList: async function (sid, db, showSystem, showNormal, showAttribute, showInfo, showPermission, server) {
		let url = server + '/database/cubes?sid='+sid+'&database='+db+'&show_system='+showSystem+'&show_normal='+showNormal+'&show_attribute='+showAttribute+'&show_info='+showInfo+'&show_permission='+showPermission;
		let response = await makeRequest("GET", url);
		let cubes = [];
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dimList = [];
			if(str[3]){
				dimList = str[3].split(",").map(x=> parseInt(x));
			}
			let cube = {
				id: str[0],
				name: str[1],
				dimNumber: str[2], //количество измерений
				cellNumber: str[4], //количество ячеек
				filledCellNumber: str[5],
				status: str[6],
				type: str[7],
				dimensionsList: dimList,
				dimensions: []
			}
			if (cube.id) {
				cube.name = cube.name.replace(/"/g, "");
				cubes[cube.id]=cube;
			}
		}
		cubes = cubes.filter(cube => cube != null)
		return cubes
	},

	getCubeByName: async function (sid, db, cubeName, server) {
		let url = server + '/database/cubes?sid='+sid+'&database='+db;
		let response = await makeRequest("GET", url);
		let cubes = [];
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dimList = [];
			if(str[3]){
				dimList = str[3].split(",").map(x=> parseInt(x));
			}
			let cube = {
				id: str[0],
				name: str[1],
				dimNumber: str[2], //количество измерений
				cellNumber: str[4], //количество ячеек
				filledCellNumber: str[5],
				dimensionsList: dimList,
			}
			if (cube.id) {
				cube.name = cube.name.replace(/"/g, "");
				cubes[cube.id]=cube;
			}
		}
		return cubes.filter(cube => cube.name == cubeName)
	},

	getCubeById: async function (sid, db, cubeId, server) {
		let url = server + '/database/cubes?sid='+sid+'&database='+db;
		let response = await makeRequest("GET", url);
		let cubes = [];
		let responseArray = response.split('\n');
		for (let i in responseArray){
			let str = responseArray[i].split(";");
			let dimList = [];
			if(str[3]){
				dimList = str[3].split(",").map(x=> parseInt(x));
			}
			let cube = {
				id: str[0],
				name: str[1],
				dimNumber: str[2], //количество измерений
				cellNumber: str[4], //количество ячеек
				filledCellNumber: str[5],
				dimensionsList: dimList,
			}
			if (cube.id) {
				cube.name = cube.name.replace(/"/g, "");
				cubes[cube.id]=cube;
			}
		}
		let cube = cubes.filter(cube => cube.id == cubeId)
		return cube[0]
	},

	cellExport: async function (sid, db, cube, area, blocksize, userules, baseonly, skipempty, server){
		let url = server + '/cell/export?sid='+sid+'&database='+db+'&cube='+cube+'&area='+area+'&blocksize='+blocksize+'&use_rules='+userules+'&base_only='+baseonly+'&skip_empty='+skipempty;
		let response = await makeRequest("GET", url);
		let responseArray = response.split('\n');
		return responseArray
	},

	//get not null elements aryays
	getNNElements: async function (responseArray){
		let notNullElements = []
		for(let i in responseArray){
			let path = responseArray[i].split(";")[3]
			if(path){
				path = path.split(",")
				for(let j in path){
				if (notNullElements[j]){
					if(!notNullElements[j].includes(parseInt(path[j])) ) notNullElements[j].push( parseInt(path[j]) )
				}
				else notNullElements[j] = [ parseInt(path[j]) ]
				}        
			}
		}
		return notNullElements
	},

	getElementsName: async function(elementsArray, dimElements){
		let result = [];
		for(let i in elementsArray){
			let element = dimElements.filter(function(e){ return (e.id==elementsArray[i]) })
			result.push(element[0]['name'])
			//result[elementsArray[i]] = element[0]['name']
		}
		return result;
	},

	parsePaloExportToJSON: async function (responseArray, cubeDimList, dimElements) {
		let jsonData = [];
		for(let i in responseArray){
				let path = responseArray[i].split(";")[3]
				if(path){
				jsonData[i] = {}
				path = path.split(",")
				for(let j in path){
					//jsonData[i][dimList[j].toString()] = path[j]
					let dimId = cubeDimList[j]
					//console.log('dimId: ', dimId)
					let element = dimElements[dimId].filter(function(e){ return (e.id==path[j]) })
					jsonData[i][dimId] = element[0]['name']
				}        
				jsonData[i]['value'] = responseArray[i].split(";")[2]
				}
			}
		return jsonData
	},

	parsePaloExportToArray: async function (responseArray, cubeDimList, dimElements) {
		let arrayData = [];
		for(let i in responseArray){
				let path = responseArray[i].split(";")[3]
				if(path){
				arrayData[i] = []
				path = path.split(",")
				for(let j in path){
					let dimId = cubeDimList[j]
					let element = dimElements[dimId][path[j]]
					arrayData[i].push(element.name)
					//let element = dimElements[dimId].filter(function(e){ return (e.id==path[j]) })
					//arrayData[i].push(element[0]['name'])
				}        
				arrayData[i].push(responseArray[i].split(";")[2])
				
				}
			}
		return arrayData
	},

	parsePaloExport_old: async function (responseArray, dimList, elements) {
		let jsonData = []
		let slices = []
		let time = Date.now();

		
		for(let i in responseArray){
			let path = responseArray[i].split(";")[3]
			if(path){
			path = path.split(",")
			let responseElements = []
			let slice = {}
			for(let j in path){
				responseElements[j] = {
				dimId: dimList[j].toString(),
				element: path[j]
				}
				slice[dimList[j].toString()] = path[j]
				
			}        

			jsonData[i] = {
				path: responseElements,
				value: responseArray[i].split(";")[2]
			}
			slice["value"] = responseArray[i].split(";")[2]
			slices[i] = slice
			
			}

		}
		
		//console.log('jsonData ready: ', time1)
		//console.log(jsonData);

		
		let cols = elements.filter(e=>e.type=="col")
		cols = cols[0];    
		let rows = elements.filter(e=>e.type=="row")
		rows = rows[0];

		if(rows.elementsList == '*'){
		//rows.elementsList = slices.reduce((acc,elem)=>acc.add(elem[rows.dimId]), new Set())
		let list = []
		for(let i=0; i<slices.length; i++){
			let row = slices[i]
			if(list.indexOf(row[rows.dimId])<0){
			list.push(Number(row[rows.dimId]))
			}
		}
		rows.elementsList = list
		}

		let time2 = Date.now() - time;
		console.log('rows ready: ', time2)

		//console.log("rows: ", rows)
		//console.log("cols: ", cols)

		let result = []
		rows.elementsList.forEach(function(rowElement,dimId,arr){
		result[result.length] = slices.filter(function(e){return (e[rows.dimId]==rowElement) })
		result[result.length-1].sort(function(a,b){
			let indexA = cols.elementsList.indexOf(Number(a[cols.dimId]))
			let indexB = cols.elementsList.indexOf(Number(b[cols.dimId]))
			return indexA - indexB
		})
		});


		let time3 = Date.now() - time;
		console.log('result array ready: ', time3)
		console.log(result);


		let valueArray = result.map(row => row.map(e => e.value))
		console.log('valueArray length: ', valueArray.length);
		console.log('valueArray: ', valueArray);
		let time4 = Date.now() - time;
		console.log('value array ready: ', time4)
		

		return valueArray
	},

	cellReplaceByName: async function (sid, db, cube, namePath, value, server){
		let splash = '0'
		let url = server + '/cell/replace?sid='+sid+'&database='+db+'&cube='+cube+'&name_path='+namePath+'&value='+value+'&splash='+splash;
		url = encodeURI(url)
		let response = await makeRequest("GET", url);
		response = response.split(';');
		return response[0]
	},

	cellReplace: async function (sid, db, cube, path, value, server){
		let splash = '0'
		let url = server + '/cell/replace?sid='+sid+'&database='+db+'&cube='+cube+'&path='+path+'&value='+value+'&splash='+splash;
		url = encodeURI(url)
		let response = await makeRequest("GET", url);
		response = response.split(';');
		return response[0]
	},

	cellReplaceBulk: async function (sid, db, cube, paths, values, server){
		const splash = '0'
		let url = server + '/cell/replace_bulk?sid='+sid+'&database='+db+'&cube='+cube+'&paths='+paths+'&values='+values+'&splash='+splash;
		let urlSaveCubeData = server + '/cube/save?sid='+sid+'&database='+db+'&cube='+cube;
		
		url = encodeURI(url)
		urlSaveCubeData = encodeURI(urlSaveCubeData)

		let response = await makeRequest("GET", url);
		response = response.split(';');

		//let response2 = await makeRequest("GET", urlSaveCubeData);

		return response[0]
	},


}




