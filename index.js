// A "closer to real-life" app example
// using 3rd party middleware modules
// P.S. MWs calls be refactored in many files

// long stack trace (+clarify from co) if needed
// TRACE=true node index.js
if (process.env.TRACE) {
	require('./libs/trace');
}
const Koa = require('koa');
const app = new Koa();
const config = require('./config/default.js');
const path = require('path');
const fs = require('fs');
const palo =  require('./libs/palo.js')
const utils =  require('./libs/utils.js')

const body = require("koa-body")

const handlers = fs.readdirSync(path.join(__dirname, 'handlers')).sort();

//app.use(body({ formidable: { maxFieldsSize: 10 * 1024 * 1024 } }))

handlers.forEach(handler => {
	const h = require('./handlers/' + handler);
	h.init(app);
});

// // can be split into files too
const Router = require('koa-router');
const router = new Router();

//import * as React from 'react';
//import * as ReactDOMServer from 'react-dom/server';
//import TestRequest from './components/TestRequest';

router.get('/views', async function(ctx, next) {
	let count = ctx.session.count || 0;
	ctx.session.count = ++count;

	ctx.body = ctx.render('./templates/index.pug', {
	user: 'John',
	count: count
	});
});

router.get('/static', async function(ctx, next) {
	ctx.body = ctx.render('./public/static.html');
});

router.post('/canvas', async function(ctx, next) {
	console.log('request: ', ctx.request.body)
	let sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	ctx.session.sid = sid
	ctx.body = {sid: sid}

	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let elements = ctx.request.body.elements
	let cube = await palo.getCubeById(ctx.session.sid, dbId, cubeId, config.paloServer)
	cube = cube[0]
	console.log('cube: ', cube);

	
	let path = await utils.makePathArea(cube, elements);
	
	let blocksize = 300000;
	let skipempty = 1;
	let responseArray = await palo.cellExport(ctx.session.sid, dbId, cubeId, path, blocksize, skipempty, config.paloServer)
	let jsonData = await palo.parsePaloExport(responseArray, cube.dimensionsList, elements)

	ctx.body = jsonData
});



// параметр ctx.params
// см. различные варианты https://github.com/pillarjs/path-to-regexp
//   - по умолчанию 1 элемент пути, можно много *
//   - по умолчанию обязателен, можно нет ?
//   - уточнение формы параметра через regexp'ы

router.post('/palo/savedata', async function(ctx, next) {
	const jsonData = ctx.request.body.data
	fs.writeFile("export_data.json", JSON.stringify(jsonData, null, 2), function(err) {
	    if (err) {
	        console.log(err);
	    }
	});
	ctx.body = "Save success";
});


router.post('/r/getMatrix', async function(ctx, next) {
	const jsonData = ctx.request.body.data
	const axios = require('axios')

	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+'_'+time;
	
	// fs.writeFile("logs/"+dateTime+"_request.json", JSON.stringify(jsonData, null, 2), function(err) {
	//     if (err) {
	//         console.log(err);
	//     }
	// });

	const getRData = async () => {
		try {
			return await axios.post('http://127.0.0.1:8888/tableProcessor', jsonData)
		} catch (error) {
	    	console.error(error)
		}
	}

	let res = await getRData()	
	let data = res.data
	
	ctx.body = data;
});




router.get('/palo/test', async function(ctx, next) {
	const url = 'http://127.0.0.1:7777/server/login?user=admin2&password=21232f297a57a5a743894a0e4a801fc3'
	const params = {
		login: ctx.request.body.login,
		pass: ctx.request.body.pass
	}
	const sid = await palo.getPaloAPI(url);

	ctx.body = "Test success"+sid;
});

router.get('/user/:user', async function(ctx, next) {
	ctx.body = "Hello, " + ctx.params.user;
});

router.get('/', async function(ctx) {
	ctx.redirect('/views');
});

router.post('/palo/login', async function(ctx, next) {
	const func = function () { return palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer) }
	const sid = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
	ctx.session.sid = sid
	ctx.session.user = ctx.request.body.login
	ctx.session.pass = ctx.request.body.pass
	ctx.body = {sid}
	
});

router.post('/palo/dblist', async function(ctx, next) {
	const func = function (sid) { return palo.getDBList(sid, config.paloServer) }
	ctx.body = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
});


router.post('/palo/dimensions', async function(ctx, next) {
	const body = ctx.request.body
	const show_normal = body.showNormal ? body.showNormal : 1
	const show_system = body.showSystem ? body.showSystem : 0
	const show_attribute = body.showAttribute ? body.showAttribute : 0
	const show_info = body.showInfo ? body.showInfo : 0
	const show_permission = body.showPermission ? body.showPermission : 1

	const func = function (sid) { return palo.getDimList(sid, ctx.request.body.db, show_system, show_normal, show_attribute, show_info, show_permission, config.paloServer) }
	ctx.body = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
});

router.post('/palo/dimElements', async function(ctx, next) {
	const body = ctx.request.body
	const db = body.db
	const dim = body.dim
	const show_permission = body.showPermission ? body.showPermission : 1
	
	const getDimElements = function (sid) { return palo.getDimElements(sid, db, dim, show_permission,config.paloServer) }
	const elements = await palo.getPaloAPI(ctx, getDimElements, palo.getSid, config.paloServer)
	ctx.body = elements

});


router.post('/palo/dimById', async function(ctx, next) {
	// let sid = ctx.session.sid || ctx.request.body.sid
	// if(ctx.request.body.login && ctx.request.body.pass){
	// 	sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	// }
	const func = function (sid) { return palo.getDimById(sid, ctx.request.body.db, ctx.request.body.dim, config.paloServer) }
	ctx.body = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
});

router.post('/palo/cubes', async function(ctx, next) {
	const body = ctx.request.body
	const show_system = body.showSystem ? body.showSystem : 0
	const show_normal = body.showNormal ? body.showNormal : 1
	const show_attribute = body.showAttribute ? body.showAttribute : 0
	const show_info = body.showInfo ? body.showInfo : 0
	const show_permission = body.showPermission ? body.showPermission : 0
	 
	
	const func = function (sid) { return palo.getCubeList(sid, ctx.request.body.db, show_system, show_normal, show_attribute, show_info, show_permission, config.paloServer) }
	let cubeList = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
	ctx.body = cubeList
});

router.post('/palo/cubes/:cubeName', async function(ctx, next) {
	// let sid = ctx.session.sid || ctx.request.body.sid
	// if(ctx.request.body.login && ctx.request.body.pass){
	// 	sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	// }
	const func = function (sid) { return palo.getCubeByName(sid, ctx.request.body.db, ctx.params.cubeName, config.paloServer) }
	ctx.body = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
});

router.post('/palo/cubeById', async function(ctx, next) {
	const func = function (sid) { return palo.getCubeById(sid, ctx.request.body.db, ctx.request.body.cube, config.paloServer) }
	ctx.body = await palo.getPaloAPI(ctx, func, palo.getSid, config.paloServer)
});

router.post('/palo/cubeDims', async function(ctx, next) {
	const getCubeById = function (sid) { return palo.getCubeById(sid, ctx.request.body.db, ctx.request.body.cube, config.paloServer) }
	const showSystem = 0
	const showNormal = 1
	const showAttribute = 0
	const showInfo = 0
	const showPermission = 0

	const getDimList = function (sid) { 
		return palo.getDimList(sid, ctx.request.body.db, showSystem, showNormal, showAttribute, showInfo, showPermission, config.paloServer)
	}

	const cube = await palo.getPaloAPI(ctx, getCubeById, palo.getSid, config.paloServer)
	const dims = await palo.getPaloAPI(ctx, getDimList, palo.getSid, config.paloServer)
	
	const cubeDimList = cube.dimensionsList
	let result = []
	for(let i in cubeDimList) {
		let dim = dims.filter( e => e.id == cubeDimList[i])
		result.push(dim[0])
	}
	ctx.body = result
});


router.post('/palo/data', async function(ctx, next) {

	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let elements = ctx.request.body.elements
	const getCubeById = function(sid){return palo.getCubeById(sid, dbId, cubeId, config.paloServer)}
	let cube = await palo.getPaloAPI(ctx, getCubeById, palo.getSid, config.paloServer)
	let path = await utils.makePathArea(cube, elements);
	let blocksize = 300000;
	let skipempty = 1;
	let baseonly = 1;
	const cellExport = function(sid){return palo.cellExport(sid, dbId, cubeId, path, blocksize, baseonly, skipempty, config.paloServer)}
	let responseArray = await palo.getPaloAPI(ctx, cellExport, palo.getSid, config.paloServer)
	//let dimsElements = await palo.getDimsElements(sid, dbId, cube.dimensionsList, config.paloServer)
	let arrayData = await palo.parsePaloExportToArray(responseArray, cube.dimensionsList, dimsElements)
	ctx.body = arrayData
});

router.post('/palo/cellExport', async function(ctx, next) {

	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let path= ctx.request.body.path
	let blocksize = ctx.request.body.blocksize ? ctx.request.body.blocksize : 300000;
	let userules = ctx.request.body.userules ? ctx.request.body.userulesm : 1;
	let skipempty = ctx.request.body.skipempty ? ctx.request.body.skipempty : 1;
	let baseonly = ctx.request.body.baseonly ? ctx.request.body.baseonly : 0;
	

	const cellExport = function(sid){return palo.cellExport(sid, dbId, cubeId, path, blocksize, userules, baseonly, skipempty, config.paloServer)}
	let responseArray = await palo.getPaloAPI(ctx, cellExport, palo.getSid, config.paloServer)
	ctx.body = responseArray
});

router.post('/palo/setData', async function(ctx, next) {

	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let path= ctx.request.body.path
	let value = ctx.request.body.value
	const cellReplace = function(sid){return palo.cellReplace(sid, dbId, cubeId, path, value, config.paloServer)}
	let responseArray = await palo.getPaloAPI(ctx, cellReplace, palo.getSid, config.paloServer)
	ctx.body = responseArray
});

router.post('/palo/setDataBulk', async function(ctx, next) {

	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let paths= ctx.request.body.paths
	let values = ctx.request.body.values
	const cellReplaceBulk = function(sid){return palo.cellReplaceBulk(sid, dbId, cubeId, paths, values, config.paloServer)}
	let responseArray = await palo.getPaloAPI(ctx, cellReplaceBulk, palo.getSid, config.paloServer)
	ctx.body = responseArray
});

router.post('/palo/nn_elements', async function(ctx, next) {

	let sid = ctx.session.sid || ctx.request.body.sid
	if(ctx.request.body.login && ctx.request.body.pass){
		sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	}
	let startTime = Date.now()
	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let elements = ctx.request.body.elements
	let cube = await palo.getCubeById(sid, dbId, cubeId, config.paloServer)
	console.log('Init t1: ', Date.now() - startTime)
	let path = await utils.makePathArea(cube, elements);
	console.log('Make path t2: ', Date.now() - startTime)
	
	const blocksize = 300000;
	const userules = 1;
	const skipempty = 1;
	const baseonly = 1;


	let responseArray = await palo.cellExport(sid, dbId, cubeId, path, blocksize, userules, baseonly, skipempty, config.paloServer)
	console.log('cell export t3: ', Date.now() - startTime)
	console.log('cell export response length: ', responseArray.length)

	let dimsElements = await palo.getDimsElements(sid, dbId, cube.dimensionsList, config.paloServer)
	console.log('get dims elements t4: ', Date.now() - startTime)
	console.log('get dims response length: ', dimsElements.length)
	
	let arrayData = await palo.parsePaloExportToArray(responseArray, cube.dimensionsList, dimsElements)
	//console.log('arrayData: ', arrayData)
	
	console.log('get elements names t5: ', Date.now() - startTime)
	let notNullElements = await palo.getNNElements(responseArray)
	console.log('get not null elements list t6: ', Date.now() - startTime)
	console.log('notNullElements length t6: ', notNullElements.length)

	let nnElements = []
	for(let i in notNullElements){
		let dimId = cube.dimensionsList[i]
		const show_permission = 0
		let dimElements = await palo.getDimElements(sid, dbId, dimId, show_permission, config.paloServer)
		let names = await palo.getElementsName(notNullElements[i], dimElements)
		nnElements[i] = {
			dimId: dimId,
			elementsCount: notNullElements[i].length,
			elements: notNullElements[i],
			elementsName: names
		}
	}

	console.log('prepare result t7: ', Date.now() - startTime)
	console.log('result length t7: ', nnElements.length)
	
	let result = {
		arraySize: responseArray.length,
		data: arrayData,
		elements: nnElements
	}
	console.log('responseArray.length: ',responseArray.length)
	ctx.body = result
	
		
	
});

router.post('/palo/jsondata', async function(ctx, next) {
	let sid = ctx.session.sid || ctx.request.body.sid
	if(ctx.request.body.login && ctx.request.body.pass){
		sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	}
	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let elements = ctx.request.body.elements
	let cube = await palo.getCubeById(sid, dbId, cubeId, config.paloServer)
	let path = await utils.makePathArea(cube, elements);
	let blocksize = 300000;
	let userules = 1;
	let skipempty = 1;
	let baseonly = 1;
	let responseArray = await palo.cellExport(sid, dbId, cubeId, path, blocksize, userules, baseonly, skipempty, config.paloServer)
	let dimsElements = await palo.getDimsElements(sid, dbId, cube.dimensionsList, config.paloServer)
	let jsonData = await palo.parsePaloExportToJSON(responseArray, cube.dimensionsList, dimsElements)
	ctx.body = jsonData
});

router.post('/palo/testdata', async function(ctx, next) {
	let sid = ctx.session.sid || ctx.request.body.sid
	if(ctx.request.body.login && ctx.request.body.pass){
		sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	}
	let rows = ctx.request.body.rows
	let cols = ctx.request.body.cols
	let testData = []
	for(let i=0;i<rows;i++){
	let row = []
	for(let j=0;j<cols;j++){
		row[j] = Math.random().toString()
	}
	testData.push(row)
	}
	console.log('testData rows: ', testData.length)
	ctx.body = testData
});


router.post('/palo/replaceValue', async function(ctx, next) {
	let sid = ctx.session.sid || ctx.request.body.sid
	if(ctx.request.body.login && ctx.request.body.pass){
		sid = await palo.getSid(ctx.request.body.login, ctx.request.body.pass, config.paloServer);
	}
	let dbId = ctx.request.body.db
	let cubeId = ctx.request.body.cube
	let namePath = ctx.request.body.namePath
	let value = ctx.request.body.value
	
	let response = await palo.cellReplaceByName(sid, dbId, cubeId, namePath, value, config.paloServer)
	console.log('replace status: ', response)
	ctx.body = response
});


router.post('/palo/:dbName/:cubeName', async function(ctx, next) {
	ctx.body = '---'+ctx.params.dbName+' : '+ctx.params.cubeName+'---'
});



app.use(router.routes());

app.listen(config.port);
