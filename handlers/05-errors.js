
exports.init = app => app.use(async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    console.log('----','error: ',e.message)
    if (e.status) {
      // could use template methods to render error page
      ctx.body = e.message;
      ctx.status = e.status;
    } else {
      ctx.body = e.message;
      ctx.status = 500;
    }

  }
});
