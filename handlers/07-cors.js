
// Parse application/json, application/x-www-form-urlencoded
// NOT form/multipart!
const cors = require('@koa/cors');

// ctx.request.body = {name: '', surname: '', ...}
exports.init = app => app.use(cors());
