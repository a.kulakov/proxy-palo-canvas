module.exports = {
  // secret data can be moved to env variables
  // or a separate config
  port: 4001,
  secret: 'mysecret',
  root: process.cwd(),
  paloServer: 'http://127.0.0.1:7777'
};
